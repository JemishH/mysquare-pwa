import config from "../config";
import { errorString } from "utils/constant";
import { errorAlert } from "utils/alert";
import { getAppVersion, getDeviceInfo } from "utils/globalFunction";

function PostRequest(path, params, isErrorHandle) {
  const requestOptions = {
    method: "POST",
    headers: {
      Accept: "application/json",
      build_version: getAppVersion(),
      device_type: 3,
      device_token: "",
      "Access-Control-Allow-Origin": "*",
      "Content-Type": "application/json",
      deviceInfo: JSON.stringify(getDeviceInfo()),
    },
    body: JSON.stringify(params),
  };

  return fetch(`${config.LIVE_URL}/api${path}`, requestOptions)
    .then((response) => response.json())
    .then((data) => {
      if (data.status === 200) {
        data.isSuccess = true;
        return data;
      } else {
        return getErrorData(data, isErrorHandle);
      }
    })
    .catch((error) => {
      return getErrorData(error, isErrorHandle);
    });
}

function getRequest(path) {
  const requestOptions = {
    method: "GET",
    headers: {
      build_version: getAppVersion(),
      Accept: "application/json",
      "Content-Type": "application/json",
      deviceInfo: JSON.stringify(getDeviceInfo()),
    },
  };

  return fetch(`${config.BASE_URL}${path}`, requestOptions)
    .then((response) => response.json())
    .then((data) => {
      if (data.status === 200) {
        data.isSuccess = true;
        return data;
      } else {
        return getErrorData(data, true);
      }
    })
    .catch((error) => {
      return getErrorData(error, true);
    });
}

const getErrorData = (data, isHandle) => {
  if (isHandle) {
    if (data.message !== "No profile found") {
      errorAlert(data.message || errorString.catchError);
    }
  }
  return (data = {
    isSuccess: false,
    isStore: false,
    message: data.message || errorString.catchError,
  });
};

export const apiService = {
  PostRequest,
  getRequest,
};
