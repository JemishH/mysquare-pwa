const getEnvironment = () => {
  switch (process.env.NEXT_PUBLIC_PROJECT_ENV) {
    case "production":
      return "production";
    case "development":
      return "development";
    default:
      return "testing";
  }
};

const getLiveUrl = () => {
  console.log("getLiveUrl.", process.env.NEXT_PUBLIC_PROJECT_ENV);

  switch (process.env.NEXT_PUBLIC_PROJECT_ENV) {
    case "production":
      return "https://mysquare.us";
    case "development":
      return "https://dev.mysquare.us";
    default:
      return "http://localhost:3001";
  }
};

const Url = {
  liveUrl: getLiveUrl(),
};

export default {
  LIVE_URL: `${Url.liveUrl}`,
  environment: getEnvironment(),
};
