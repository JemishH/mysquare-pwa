import config from "../config";
const design1 = `${config.LIVE_URL}/assets/images/design_01.png`;

const keywords = {
  landingPage: "Architectural Firm",
  Projects:
    "how it works,howitworks,FeetFinder howitworks,FeetFinder how it works",
  contact: "MySquare Contact, MySquare Contact Us, Contact, Contact Us",
  SecretsOfProject: "",
};
export const metaDetail = {
  landing: {
    title: "MySquare",
    h1: "Architectural Firm",
    keywords: keywords.landingPage,
    canonical: "https://mysquare.us",
    desc: "",
  },
  Projects: {
    title: "Projects - MySquare",
    canonical: "https://mysquare.us/projects",
    desc: "",
  },
  SecretsOfProject: {
    title: "Secrets Of Project - MySquare",
    canonical: "https://mysquare.us/secrets",
    desc: "",
  },
  contact: {
    title: "Contact Us - MySquare",
    canonical: "https://mysquare.us/",
    h1: "Contact Us",
    desc: "Contact with feet finder",
  },
};

export const staticImagesUrl = {
  SITE_LOGO: "/mySquare.png",
  TRANSPARANT_SITE_LOGO: "",
};

export const sliderTestImages = [
  {
    id: 0,
    path: design1,
  },
  {
    id: 1,
    path: design1,
  },
  {
    id: 2,
    path: design1,
  },
];

const projectImg1 = `${config.LIVE_URL}/assets/images/project_01.png`;
const projectImg2 = `${config.LIVE_URL}/assets/images/project_02.png`;
export const staticProjectListData1 = [
  {
    id: 0,
    projectName: "Bathrooms with accent walls",
    projectDesc: "Bathrooms with accent walls",
    imageUrl: projectImg1,
  },
  {
    id: 1,
    projectName: "Bathrooms with accent walls",
    projectDesc: "Bathrooms with accent walls",
    imageUrl: projectImg2,
  },
];
export const staticProjectListData = [
  {
    id: 0,
    projectName: "Bathrooms with accent walls",
    projectDesc: "Bathrooms with accent walls",
    imageUrl: projectImg1,
  },
  {
    id: 1,
    projectName: "Bathrooms with accent walls",
    projectDesc: "Bathrooms with accent walls",
    imageUrl: projectImg2,
  },
  {
    id: 2,
    projectName: "Bathrooms with accent walls",
    projectDesc: "Bathrooms with accent walls",
    imageUrl: projectImg1,
  },
  {
    id: 3,
    projectName: "Bathrooms with accent walls",
    projectDesc: "Bathrooms with accent walls",
    imageUrl: projectImg2,
  },
  {
    id: 4,
    projectName: "Bathrooms with accent walls",
    projectDesc: "Bathrooms with accent walls",
    imageUrl: projectImg1,
  },
  {
    id: 5,
    projectName: "Bathrooms with accent walls",
    projectDesc: "Bathrooms with accent walls",
    imageUrl: projectImg2,
  },
  {
    id: 6,
    projectName: "Bathrooms with accent walls",
    projectDesc: "Bathrooms with accent walls",
    imageUrl: projectImg1,
  },
  {
    id: 7,
    projectName: "Bathrooms with accent walls",
    projectDesc: "Bathrooms with accent walls",
    imageUrl: projectImg2,
  },
  {
    id: 8,
    projectName: "Bathrooms with accent walls",
    projectDesc: "Bathrooms with accent walls",
    imageUrl: projectImg1,
  },
  {
    id: 9,
    projectName: "Bathrooms with accent walls",
    projectDesc: "Bathrooms with accent walls",
    imageUrl: projectImg2,
  },
];

export const successMessages = {
  emailSentSuccess: "Email sent successfully",
  msgSentSuccess: "Your message sent successfully",
};

export const errorMessages = {
  nameRequired: "Name is required",
  emailRequired: "Email is required",
  validEmail: "Enter valid email",
  messageRequired: "Message is required",
};

export const errorString = {
  catchError: "Something went wrong",
};
