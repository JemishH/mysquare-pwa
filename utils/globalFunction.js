import { AppVersions } from "./appVersion";
import {
  browserName,
  browserVersion,
  engineName,
  engineVersion,
  deviceType,
  mobileModel,
  mobileVendor,
  fullBrowserVersion,
  osName,
  osVersion,
} from "react-device-detect";

export const getAppVersion = () => {
  switch (process.env.NEXT_PUBLIC_PROJECT_ENV) {
    case "production":
      return AppVersions.production;
    case "staging":
      return AppVersions.staging;
    default:
      return AppVersions.development;
  }
};

export const getDeviceInfo = () => {
  const deviceInfo = {};
  if (browserVersion && browserVersion.length > 0) {
    deviceInfo.browserVersion = browserVersion;
  }
  if (browserName && browserName.length > 0) {
    deviceInfo.browserName = browserName;
  }
  if (engineVersion && engineVersion.length > 0) {
    deviceInfo.engineVersion = engineVersion;
  }
  if (engineName && engineName.length > 0) {
    deviceInfo.engineName = engineName;
  }
  if (deviceType && deviceType.length > 0) {
    deviceInfo.deviceType = deviceType;
  }
  if (mobileVendor && mobileVendor.length > 0) {
    deviceInfo.mobileVendor = mobileVendor;
  }
  if (mobileModel && mobileModel.length > 0) {
    deviceInfo.mobileModel = mobileModel;
  }
  if (osName && osName.length > 0) {
    deviceInfo.osName = osName;
  }
  if (osVersion && osVersion.length > 0) {
    deviceInfo.osVersion = osVersion;
  }
  if (fullBrowserVersion && fullBrowserVersion.length > 0) {
    deviceInfo.fullBrowserVersion = fullBrowserVersion;
  }
  return deviceInfo;
};

// Validation Check
export const isValidEmail = (email) => {
  const expression =
    /(?!.*\.{2})^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([\t]*\r\n)?[\t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([\t]*\r\n)?[\t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
  return expression.test(String(email).toLowerCase());
};
