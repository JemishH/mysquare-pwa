import React, { useEffect, useState } from "react";
import usePrevious from "../../utils/customHook/usePrevious";
import PropTypes from "prop-types";
import InlineSVG from "svg-inline-react";

import { amimationlogo } from "assets/images/svgs/inlinesvg";
const LoaderComponent = (props) => {
  const [isLoading, setIsLoading] = useState(props.isLoading);
  const prevProps = usePrevious(props);
  useEffect(() => {
    if (isLoading) {
      document.body.classList.add("loaderactive");
    } else {
      document.body.classList.remove("loaderactive");
    }
  }, []);

  useEffect(() => {
    const newProps = props;
    if (newProps !== prevProps) {
      if (newProps.isLoading !== isLoading) {
        if (newProps.isLoading) {
          document.body.classList.add("loaderactive");
        } else {
          document.body.classList.remove("loaderactive");
        }
        setIsLoading(newProps.isLoading);
      }
    }
  }, [props]);

  return isLoading ? (
    <div className="ff_fullView">
      {isLoading && <InlineSVG src={amimationlogo} />}
    </div>
  ) : (
    <div />
  );
};

LoaderComponent.propTypes = { isLoading: PropTypes.bool.isRequired };

export default LoaderComponent;
