import React, { useEffect } from "react";
import "styles/component/project_detail_component.module.scss";
import { Container, Row, Col } from "react-bootstrap";
import Aos from "aos";
import "aos/dist/aos.css";
import PropTypes from "prop-types";
import Router from "next/router";
import ProgressiveNextImage from "components/ProgressiveNextImageComponent";

const ProjectDetailComponent = (props) => {
  useEffect(() => {
    Aos.init({
      offset: 200,
      duration: 1500,
      easing: "ease-in-sine",
      delay: 100,
    });

    if (!props?.query?.project) {
      Router.back();
    }
  }, []);

  return (
    <section
      className="projectdetailmain"
      data-aos="fade-up"
      data-aos-delay="100"
    >
      <Container>
        <Row>
          <Col md={12}>
            <h1>Glimpses of our Creative Accomplishments</h1>
            <div className="subtitle">
              We offer a creative process that empathizes with our clients.
              Finally, add the magic of our creativity in our work to see the
              smile on their face. It makes us are proud to showcase that Art of
              design.
            </div>
          </Col>
        </Row>
        <Row>
          <Col md={12}>
            <div className="listmain">
              <div className="listimagemain">
                <ProgressiveNextImage
                  imageSrc={"/assets/images/project_02.png"}
                />
                {/* <ImageComponent url="assets/images/project_02.png" /> */}
                <div className="inner-block">
                  <div className="slider-top-right" />
                </div>
              </div>
              <div className="detail_content_main">
                <div className="project_heading">
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed
                  do eiusmod tempor incididunt ut labore et dolore.
                </div>
                <div className="project_detail_content">
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed
                  do eiusmod tempor incididunt ut labore et dolore.Lorem ipsum
                  dolor sit amet, consectetur adipiscing elit, sed do eiusmod
                  tempor incididunt ut labore et dolore.Lorem ipsum dolor sit
                  amet, consectetur adipiscing elit, sed do eiusmod tempor
                  incididunt ut labore et dolore.Lorem ipsum dolor sit amet,
                  consectetur adipiscing elit, sed do eiusmod tempor incididunt
                  ut labore et dolore.
                </div>
              </div>
            </div>
          </Col>
        </Row>

        <Row>
          <Col md={6}>
            <div className="listmain">
              <div className="listimagemain">
                <ProgressiveNextImage
                  imageSrc={"/assets/images/project_02.png"}
                />
                {/* <ImageComponent url="assets/images/project_02.png" /> */}
                <div className="inner-block">
                  <div className="slider-top-right"></div>
                </div>
              </div>
              <div className="projectname">Bathrooms with accent walls</div>
            </div>
          </Col>
          <Col md={6}>
            <div className="listmain">
              <div className="listimagemain">
                <ProgressiveNextImage
                  imageSrc={"/assets/images/project_02.png"}
                />
                {/* <ImageComponent url="assets/images/project_02.png" /> */}
                <div className="inner-block">
                  <div className="slider-top-right"></div>
                </div>
              </div>
              <div className="projectname">Bathrooms with accent walls</div>
            </div>
          </Col>
        </Row>
        <Row>
          <Col md={12}>
            <div className="">
              <div className="project_detail_content">
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                eiusmod tempor incididunt ut labore et dolore.Lorem ipsum dolor
                sit amet, consectetur adipiscing elit, sed do eiusmod tempor
                incididunt ut labore et dolore.Lorem ipsum dolor sit amet,
                consectetur adipiscing elit, sed do eiusmod tempor incididunt ut
                labore et dolore.Lorem ipsum dolor sit amet, consectetur
                adipiscing elit, sed do eiusmod tempor incididunt ut labore et
                dolore.
              </div>
            </div>
          </Col>
        </Row>
      </Container>
    </section>
  );
};

ProjectDetailComponent.propTypes = {
  pathName: PropTypes.string.isRequired,
  query: PropTypes.object.isRequired,
};

export default ProjectDetailComponent;
