import React, { useState } from "react";
import "styles/component/banner_component.module.scss";
import { Container } from "react-bootstrap";
import ProgressiveNextImage from "components/ProgressiveNextImageComponent";
import ImageComponent from "components/ImageComponent";

import InlineSVG from "svg-inline-react";

import {
  Whatasapp,
  Facebook,
  Twitter,
  Meassage,
  NavbarIcon,
} from "assets/images/svgs/inlinesvg";
import Link from "next/link";
const mySqureLogo = "/assets/images/mySquare.png";
const bannerImg = "/assets/images/banner_img.png";

const BannerComponent = () => {
  const [isMenuOpen, setIsMenuOpen] = useState(false);
  const toggleSideMenu = () => {
    setIsMenuOpen(!isMenuOpen);
  };
  return (
    <section className="bannerimgmain">
      <div className="left_nav_main">
        <nav className={`nav-main_i ${isMenuOpen ? "open" : ""}`}>
          <div className="btn-menu" onClick={() => toggleSideMenu()}>
            <div className="btn-menu_i">
              <div className="menuimg">
                <InlineSVG src={NavbarIcon} />
              </div>
            </div>
          </div>
          <div className="menu">
            <ul className="menu__list">
              <li>
                <Link href={`/projects`}>
                  <a target="_self" rel="noopener">
                    Project List
                  </a>
                </Link>
              </li>
              <li>
                <Link href={`/secrets`}>
                  <a target="_self" rel="noopener">
                    Project Secrets
                  </a>
                </Link>
              </li>
            </ul>
          </div>
        </nav>
      </div>

      <div className="header">
        <Container>
          <div className="navbar">
            <div className="brandimg">
              <ProgressiveNextImage imageSrc={mySqureLogo} />

              {/* <ImageComponent url={mySqureLogo} /> */}
              {/* <InlineSVG src={Logo_Icon} /> */}
            </div>
            <ul className="headersocialiconmain">
              <li>
                <Link href="https://facebook.com/" passHref={true}>
                  <InlineSVG src={Facebook} />
                </Link>
              </li>
              <li>
                <Link href="https://twitter.com/" passHref={true}>
                  <InlineSVG src={Twitter} />
                </Link>
              </li>
              <li>
                <Link href="https://www.messenger.com/" passHref={true}>
                  <InlineSVG src={Meassage} />
                </Link>
              </li>
              <li>
                <Link href="https://web.whatsapp.com/" passHref={true}>
                  <InlineSVG src={Whatasapp} />
                </Link>
              </li>
              <li className="navicon">
                <div className="collapseicon">
                  <InlineSVG src={NavbarIcon} />
                </div>
              </li>
            </ul>
          </div>
        </Container>
      </div>
      <div>
        <div className="banner_img">
          <ProgressiveNextImage imageSrc={bannerImg} />

          {/* <ImageComponent url={bannerImg} /> */}
        </div>
      </div>
      <div className="bannerlogoimg">
        {/* <ProgressiveNextImage imageSrc={mySqureLogo} /> */}

        <ImageComponent url={mySqureLogo} />
        {/* <InlineSVG src={Logo_Icon} /> */}
      </div>
      <div className="bannercontent">
        <h1>
          My Square is a Leading Architectural Firm in San Jose, California.
        </h1>
      </div>
    </section>
  );
};

BannerComponent.propTypes = {};

export default BannerComponent;
