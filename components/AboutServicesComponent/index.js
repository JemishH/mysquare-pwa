import React, { useEffect } from "react";
import "styles/component/AboutServicesComponent.module.scss";
import { Container, Row, Col } from "react-bootstrap";
import ProgressiveNextImage from "components/ProgressiveNextImageComponent";

import Link from "next/link";
import Aos from "aos";
import "aos/dist/aos.css";
// import Particles from "react-particles-js";
const AboutServicesComponent = () => {
  useEffect(() => {
    Aos.init({
      offset: 200,
      duration: 1500,
      easing: "ease-in-sine",
      delay: 100,
    });
  }, []);
  return (
    <section
      className="aboutmainsection"
      data-aos="fade-up"
      data-aos-delay="100"
    >
      {/* <Particles
        params={{
          particles: {
            color: {
              value: "#000",
              opacity: 0.2,
            },
            number: {
              value: 80,
            },
            size: {
              value: 2,
            },
            line_linked: {
              enable: true,
              distance: 180,
              color: "#000",
              opacity: 0.2,
              width: 1,
            },
          },
          interactivity: {
            events: {
              onhover: {
                enable: true,
                mode: "repulse",
              },
            },
          },
        }}
      /> */}
      <Container>
        <Row>
          <Col lg={5}>
            <div className="aboutsectionmain">
              <h1>Creator and Visionary</h1>
              <p className="subtitle">
                Manoratha Patel is the founder of My Square. She is a passionate
                architectural designer with an eye for the details.
              </p>
              <div className="aboutcontentmain">
                <div className="aboutimg">
                  {/* <ImageComponent url="/assets/images/IMG_3213_edited.png" /> */}
                  <ProgressiveNextImage imageSrc="/assets/images/IMG_3213_edited.png" />
                </div>
                <div className="aboutname">Manoratha Patel</div>
              </div>
            </div>
          </Col>
          <Col lg={7}>
            <div className="servicessectionmain">
              <h1>Our Services</h1>
              <ul className="tab_name_list">
                <li className="tab_name">
                  <Link href="/">
                    <a rel="noopener">
                      <span />
                      Organize
                    </a>
                  </Link>
                </li>
                <li className="tab_name">
                  <Link href="/">
                    <a rel="noopener">
                      <span />
                      Design
                    </a>
                  </Link>
                </li>
                <li className="tab_name">
                  <Link href="/">
                    <a rel="noopener">
                      <span />
                      Deliver
                    </a>
                  </Link>
                </li>
              </ul>
              <div className="servicescontentmain">
                <div className="servicesimg">
                  <ProgressiveNextImage imageSrc="/assets/images/design_1.png" />

                  {/* <ImageComponent url="assets/images/design_1.png" /> */}
                </div>
                <div className="content_main_block">
                  <div className="service_name">Organize</div>
                  <p className="subtitle">
                    We organize spaces so you can thrive and be productive.
                  </p>
                  <div className="list_main">
                    <ul>
                      <li>
                        <Link href="/">
                          <a rel="noopener">Kitchen</a>
                        </Link>
                      </li>
                      <li>
                        <Link href="/">
                          <a rel="noopener">Living Room</a>
                        </Link>
                      </li>
                      <li>
                        <Link href="/">
                          <a rel="noopener">Dining Room</a>
                        </Link>
                      </li>
                      <li>
                        <Link href="/">
                          <a rel="noopener">Bathroom</a>
                        </Link>
                      </li>
                      <li>
                        <Link href="/">
                          <a rel="noopener">Kitchen</a>
                        </Link>
                      </li>
                    </ul>
                  </div>
                  <div className="contactbuttonmain">
                    <button className={"ms-button"}>
                      <span>SET-UP PHONE CONSULTATION</span>
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </Col>
        </Row>
      </Container>
    </section>
  );
};

AboutServicesComponent.propTypes = {};

export default AboutServicesComponent;
