import React, { useEffect } from "react";
import Slider from "react-slick";
import "styles/component/design_component.module.scss";
import { Container, Row, Col } from "react-bootstrap";
import PropTypes from "prop-types";
import SlideRightArrow from "../../assets/images/right_arrow.png";
import SlideLeftArrow from "../../assets/images/left_arrow.png";
import Aos from "aos";
import ImageComponent from "components/ImageComponent";

import "aos/dist/aos.css";
const SliderComponent = ({ imageArr, isDotsAvail, isArrowShow }) => {
  useEffect(() => {
    Aos.init({
      offset: 200,
      duration: 1500,
      easing: "ease-in-sine",
      delay: 100,
    });
  }, []);
  function SampleNextArrow(props) {
    const { className, style, onClick } = props;
    return (
      <div
        className={className}
        style={{
          ...style,
          display: "block",
          background: `url(${SlideRightArrow})`,
          left: "unset",
          bottom: "-70px",
          top: "inherit",
          right: "0px",
          backgroundSize: "100%",
          width: "35px",
          height: "35px",
          backgroundRepeat: "no-repeat",
          zIndex: "1",
        }}
        onClick={onClick}
      />
    );
  }

  function SamplePrevArrow(props) {
    const { className, style, onClick } = props;
    return (
      <div
        className={className}
        style={{
          ...style,
          display: "block",
          background: `url(${SlideLeftArrow})`,
          right: "50px",
          bottom: "-70px",
          top: "inherit",
          left: "unset",
          width: "35px",
          height: "35px",
          backgroundSize: "100%",
          backgroundRepeat: "no-repeat",
          zIndex: "1",
        }}
        onClick={onClick}
      />
    );
  }
  const settings = {
    arrows: isArrowShow || false,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    setVisible: false,
    dots: isDotsAvail || false,
    nextArrow: <SampleNextArrow />,
    prevArrow: <SamplePrevArrow />,
  };

  return (
    imageArr &&
    imageArr.length > 0 && (
      <section className="designmain" data-aos="fade-up" data-aos-delay="100">
        <Container>
          <Row>
            <Col md={12}>
              <h1>Designs</h1>
            </Col>
          </Row>
          <Row>
            <Col md={12}>
              <div className="design_slider_main">
                <Slider {...settings}>
                  {imageArr.map((data, index) => {
                    return (
                      <div key={index} className="design_slide">
                        {/* <ProgressiveNextImage imageSrc={data.path} /> */}

                        <ImageComponent url={data.path} />
                      </div>
                    );
                  })}
                </Slider>
              </div>
            </Col>
          </Row>
        </Container>
      </section>
    )
  );
};

SliderComponent.propTypes = {
  imageArr: PropTypes.array.isRequired,
  isDotsAvail: PropTypes.bool.isRequired,
  isArrowShow: PropTypes.bool.isRequired,
};

export default SliderComponent;
