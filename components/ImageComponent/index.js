import React, { useState, useEffect } from "react";
// import { AltUrlType, staticImagesUrl } from "../../utils/Constant";
import mySquare from "../../assets/images/mySquare.png";

const ImageComponentWithUpdate = (props) => {
  const [url, setUrl] = useState(props.url);
  const [isError, setIsError] = useState(!props.url);

  useEffect(() => {
    if (props?.url?.trim().length > 0) {
      checkImageUrl(props.url);
    } else {
      setIsError(false);
      onError();
    }
  }, []);

  useEffect(() => {
    if (props?.url?.trim().length > 0) {
      if (props.url !== url) {
        checkImageUrl(props.url);
        setIsError(false);
      }
    }
  }, [props]);

  const checkImageUrl = async (urlData) => {
    if (urlData && urlData?.length > 0) {
      let image = new Image();
      image.onload = function () {
        setUrl(urlData);
        image = null;
      };
      image.onerror = function () {
        onError();
        image = null;
      };
      image.src = urlData;
    } else {
      onError();
    }
  };

  const onError = () => {
    setDefaultImage();
  };

  const setDefaultImage = () => {
    setUrl(mySquare);
  };

  return (
    <img
      alt=""
      src={url}
      className={`img-fluid  lazyload ${isError ? "ms_img_error" : ""}`}
      onContextMenu={(e) => e.preventDefault()}
      onMouseDown={(e) => e.preventDefault()}
    />
  );
};

export default ImageComponentWithUpdate;
