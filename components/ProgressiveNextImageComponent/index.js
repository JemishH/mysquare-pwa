/* eslint-disable no-console */
import Image from "next/image";
import React from "react";
import { staticImagesUrl } from "utils/constant";

class ProgressiveNextImage extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      isError: false,
      url: "",
    };
    this.onError = this.onError.bind(this);
  }

  // Life cycle
  static getDerivedStateFromProps(props, state) {
    if (props.imageSrc && props.imageSrc.trim().length > 0) {
      if (props.imageSrc !== state.url) {
        const url = props?.imageSrc?.trim();

        // Change in props
        return {
          url: url,
        };
      }
    } else {
      return {
        url: staticImagesUrl.SITE_LOGO,
      };
    }

    // No change to state and set default logo when url comes with "" or return null
    // return {
    //   url: props.isUserProfilePic ? staticImagesUrl.FFLogo : staticImagesUrl.ff_Logo_white_webp,
    // }
    // return null
  }

  // Event methods
  onError() {
    this.setState({
      isError: true,
    });
  }

  render() {
    const { url, isError } = this.state;
    const {
      alt,
      imageQuality,
      //  srcSet
    } = this.props;

    return (
      <Image
        src={isError ? staticImagesUrl.SITE_LOGO : url}
        alt={alt}
        layout="fill"
        quality={imageQuality || 75}
        // srcSet={srcSet || staticImagesUrl.ff_Logo_white_webp}
        onError={this.onError}
        className={`img-fluid  ${isError ? "ms_img_error" : ""}`}
      />
    );
  }
}

export default ProgressiveNextImage;
