import React, { useEffect } from "react";
import "styles/component/project_list_component.module.scss";
import { Container, Row, Col } from "react-bootstrap";
import Aos from "aos";
import "aos/dist/aos.css";
import ProgressiveNextImage from "components/ProgressiveNextImageComponent";

// import ParticleAnimation from "react-particle-animation";
// import Particles from "react-particles-js";
import Router from "next/router";
import PropTypes from "prop-types";

const ProjectListComponent = (props) => {
  // Life cycle hooks
  useEffect(() => {
    Aos.init({
      offset: 200,
      duration: 1500,
      easing: "ease-in-sine",
      delay: 100,
    });
  }, []);

  // Render methods
  // const renderParticals = () => {
  //   return (
  //     <ParticleAnimation
  //       numParticles={300}
  //       color={"r: 225, g: 225, b: 225, a: 225"}
  //       lineWidth={0.3}
  //       particleRadius={1.1}
  //       style={{
  //         position: "absolute",
  //         width: "100%",
  //         height: "100%",
  //       }}
  //     />
  //   );
  // };

  return (
    <section
      className="projectlistmain"
      data-aos="fade-up"
      data-aos-delay="100"
    >
      {/* {renderParticals()} */}
      <Container>
        <Row>
          <Col md={12}>
            <h1>Glimpses of our Creative Accomplishments</h1>
            <div className="subtitle">
              We offer a creative process that empathizes with our clients.
              Finally, add the magic of our creativity in our work to see the
              smile on their face. It makes us are proud to showcase that Art of
              design.
            </div>
          </Col>
        </Row>
        <Row>
          {props.projects?.length > 0 &&
            props.projects?.map((project, index) => {
              return (
                <Col md={6} key={index}>
                  <div
                    className="listmain"
                    onClick={() =>
                      Router.push(`/project/${project?.projectName}`)
                    }
                  >
                    <div className="listimagemain">
                      <ProgressiveNextImage imageSrc={project?.imageUrl} />
                      {/* <ImageComponent url={project?.imageUrl} /> */}
                      <div className="inner-block">
                        <div className="slider-top-right"></div>
                      </div>
                    </div>
                    <div className="projectname">
                      {project?.projectName || "Default Name"}
                    </div>
                  </div>
                </Col>
              );
            })}
          {props.pathName === "/" && (
            <Col md={12}>
              <div className="loadmorebuttonmain">
                <button
                  className={"ms-button"}
                  onClick={() => Router.push("/projects")}
                >
                  More Projects
                </button>
              </div>
            </Col>
          )}
        </Row>
      </Container>
    </section>
  );
};

ProjectListComponent.propTypes = {
  pathName: PropTypes.string,
};

export default ProjectListComponent;
