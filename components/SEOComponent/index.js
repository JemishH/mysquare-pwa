import React from "react";
import Head from "next/head";
import { staticImagesUrl } from "../../utils/constant";

const MetaSEO = ({ metaDetail }) => {
  const title = metaDetail?.title || "MySquare";
  const desc = metaDetail?.desc || "";
  const canonical = metaDetail?.canonical || "";
  const image = metaDetail?.image || staticImagesUrl.TRANSPARANT_SITE_LOGO;
  const titleIcon = metaDetail?.image || staticImagesUrl.SITE_LOGO;
  const siteKeywords = metaDetail?.keywords || "";

  return (
    <Head>
      <title>{title}</title>
      <meta name="description" content={desc} />
      {metaDetail?.keywords && <meta name="keywords" content={siteKeywords} />}
      <meta property="og:type" content="website" />
      <meta name="og:title" property="og:title" content={title} />
      <meta name="og:description" property="og:description" content={desc} />
      <meta property="og:site_name" content="Feet Finder" />
      <meta property="og:url" content={canonical} />
      <meta name="twitter:card" content="summary" />
      <meta name="twitter:title" content={title} />
      <meta name="twitter:description" content={desc} />
      <meta name="twitter:site" content="@propernounco" />
      <meta name="twitter:creator" content="@propernounco" />
      <link rel="icon" sizes="192x192" href={metaDetail?.image || titleIcon} />
      <link rel="apple-touch-icon" href={metaDetail?.image || titleIcon} />
      <meta property="og:image" content={image} />
      <meta name="twitter:image" content={image} />
      {metaDetail?.canonical && <link rel="canonical" href={canonical} />}
    </Head>
  );
};

export default MetaSEO;
