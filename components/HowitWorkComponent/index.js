import React, { useEffect } from "react";
import "styles/component/howitworkcomponent.module.scss";
import { Container, Row, Col } from "react-bootstrap";
import Aos from "aos";
import "aos/dist/aos.css";
import Link from "next/link";
const HowitWorkComponent = () => {
  useEffect(() => {
    Aos.init({
      offset: 200,
      duration: 1500,
      easing: "ease-in-sine",
      delay: 100,
    });
  }, []);
  return (
    <section
      className="howitworkmainsection"
      data-aos="fade-up"
      data-aos-delay="100"
    >
      <Container>
        <Row>
          <Col lg={6}>
            <div className="howitworkcontentmain">
              {/* <h1>Hello,</h1> */}
              <h1>My Square,</h1>

              <div className="subtitle">
                is a Creative Architectural Firm founded by Manoratha Patel. We
                specialize in custom interior design and architecture. We love
                our work and want to show the best in all designs. We will work
                with you to find the perfect solution for your space and will
                give you the best advice and training you need to get the most
                out of your design process.
              </div>
              <div className="subtitle">
                We want to help you find the right place for your business, and
                we want to make sure you are happy with your decision. When you
                hire us you will find a team of skilled professionals who will
                work with you to achieve the best results.
              </div>
            </div>
          </Col>
          <Col lg={6}>
            <div className="howitworkcontentmain1">
              <h1>Let us</h1>
              <h2>
                reveal the secret to our
                <span className="secrettext">magic!</span>
              </h2>
              {/* <h1>LET US</h1>
              <h2>
                REVEAL THE SECRET TO OUR
                <span className="secrettext">MAGIC!</span>
              </h2> */}
              <div className="howitworkbuttonmain">
                <Link href="/secrets">
                  <a rel="noopener">
                    <button className={"ms-button"}>
                      <span>How We Work</span>
                    </button>
                  </a>
                </Link>
              </div>
            </div>
          </Col>
        </Row>

        <Row>
          <Col md={12}>
            <div className="howitworktitle">
              <h2>How It Works</h2>
            </div>
          </Col>
          <Col lg={4}>
            <div className="listmain">
              <div className="projecttitle">Phone Consultation</div>
              <div className="projectdetail">
                We begin with a phone consultation to discuss your project and
                specific needs to ensure we understand you. We will answer any
                questions you might have and proceed to schedule an In-Home
                consultation. A phone consultation is free and waiting for you.
              </div>
            </div>
          </Col>
          <Col lg={4}>
            <div className="listmain">
              <div className="projecttitle">Home Consultation</div>
              <div className="projectdetail">
                A MySquare team member will arrive at the scheduled time at your
                home, business, or worksite where an action plan is created
                about your specific project. We will discuss hourly rates,
                schedule dates, take before photos, and a deposit of $150 will
                be due at the appointment time. $100 of this fee will be applied
                to your total cost.
              </div>
            </div>
          </Col>
          <Col lg={4}>
            <div className="listmain">
              <div className="projecttitle">Rates & Invoice</div>
              <div className="projectdetail">
                After your home consultation we will send you a revised quote
                with 100.00 credit towards your project. Once agreed payment is
                processed we show up to your door ready to connect you to your
                space leaving you happy and relieved.
              </div>
            </div>
          </Col>
        </Row>
      </Container>
    </section>
  );
};

HowitWorkComponent.propTypes = {};

export default HowitWorkComponent;
