import React, { useState } from "react";
import "styles/component/header_main_component.module.scss";
import { Container } from "react-bootstrap";
import ProgressiveNextImage from "components/ProgressiveNextImageComponent";

import InlineSVG from "svg-inline-react";
import {
  Whatasapp,
  Facebook,
  Twitter,
  Meassage,
  NavbarIcon,
} from "assets/images/svgs/inlinesvg";
import PropTypes from "prop-types";
import Link from "next/link";

const HeaderComponent = () => {
  const [isNavOpen, setIsNavOpen] = useState(false);
  return (
    <div className="header">
      <Container className="extramenu">
        <div className="navbar">
          <div className="brandimg">
            <Link href="/">
              <a rel="noopener">
                <ProgressiveNextImage
                  imageSrc={"/assets/images/mySquare.png"}
                />
                {/* <ImageComponent url={mySqureLogo} /> */}
              </a>
            </Link>
          </div>
          <ul className="headersocialiconmain">
            <li>
              <Link href="https://facebook.com/" passHref={true}>
                <InlineSVG src={Facebook} />
              </Link>
            </li>
            <li>
              <Link href="https://twitter.com/" passHref={true}>
                <InlineSVG src={Twitter} />
              </Link>
            </li>
            <li>
              <Link href="https://www.messenger.com/" passHref={true}>
                <InlineSVG src={Meassage} />
              </Link>
            </li>
            <li>
              <Link href="https://web.whatsapp.com/" passHref={true}>
                <InlineSVG src={Whatasapp} />
              </Link>
            </li>
            <li onClick={() => setIsNavOpen(!isNavOpen)}>
              <div className="collapseicon">
                <InlineSVG src={NavbarIcon} />
              </div>
            </li>
          </ul>
        </div>
        {isNavOpen && (
          <div className="extramenu_main">
            <ul className="menu__list">
              <li>
                <Link href={`/projects`}>
                  <a target="_self" rel="noopener">
                    Project List
                  </a>
                </Link>
              </li>
              <li>
                <Link href={`/secrets`}>
                  <a target="_self" rel="noopener">
                    Project Secrets
                  </a>
                </Link>
              </li>
            </ul>
          </div>
        )}
      </Container>
    </div>
  );
};

// Specifies the default values for props:
HeaderComponent.defaultProps = {
  pathName: "/",
};

HeaderComponent.propTypes = {
  pathName: PropTypes.string.isRequire,
};

export default HeaderComponent;
