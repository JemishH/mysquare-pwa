import React, { useEffect } from "react";
import Slider from "react-slick";
import "styles/component/review_component.module.scss";
import { Container, Row, Col } from "react-bootstrap";
import InlineSVG from "svg-inline-react";
import { RatingIcon } from "assets/images/svgs/inlinesvg";
import PropTypes from "prop-types";
import SlideRightArrow from "../../assets/images/review_right_arrow.png";
import SlideLeftArrow from "../../assets/images/review_left_arrow.png";
import Aos from "aos";
import ProgressiveNextImage from "components/ProgressiveNextImageComponent";

import "aos/dist/aos.css";

const ReviewSliderComponent = ({ imageArr, isDotsAvail, isArrowShow }) => {
  useEffect(() => {
    Aos.init({
      offset: 200,
      duration: 1500,
      easing: "ease-in-sine",
      delay: 100,
    });
  }, []);
  function SampleNextArrow(props) {
    const { className, style, onClick } = props;
    return (
      <div
        className={className}
        style={{
          ...style,
          display: "block",
          background: `url(${SlideRightArrow})`,
          bottom: "-60px",
          top: "inherit",
          left: "50px",
          backgroundSize: "100%",
          width: "35px",
          height: "35px",
          backgroundRepeat: "no-repeat",
          zIndex: "1",
        }}
        onClick={onClick}
      />
    );
  }

  function SamplePrevArrow(props) {
    const { className, style, onClick } = props;
    return (
      <div
        className={className}
        style={{
          ...style,
          display: "block",
          background: `url(${SlideLeftArrow})`,
          right: "unset",
          bottom: "-60px",
          top: "inherit",
          left: "0",
          width: "35px",
          height: "35px",
          backgroundSize: "100%",
          backgroundRepeat: "no-repeat",
          zIndex: "1",
        }}
        onClick={onClick}
      />
    );
  }
  const settings = {
    arrows: isArrowShow || false,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    setVisible: false,
    dots: isDotsAvail || false,
    nextArrow: <SampleNextArrow />,
    prevArrow: <SamplePrevArrow />,
  };

  return (
    imageArr &&
    imageArr.length > 0 && (
      <section className="reviewsmain" data-aos="fade-up" data-aos-delay="100">
        <Container>
          <Row>
            <Col md={12}>
              <h1>Reviews</h1>
            </Col>
          </Row>
          <Row>
            <Col md={12}>
              <div className="reviews_slider_main">
                <Slider {...settings}>
                  {imageArr.map((data, index) => {
                    return (
                      <div
                        className="reviews_slider_content"
                        key={`ms-review-${index}`}
                      >
                        <Row>
                          <Col lg={3}>
                            <div className="reviewimagemain" key={index}>
                              <ProgressiveNextImage imageSrc={data.path} />
                              {/* <ImageComponent url={data.path} /> */}
                            </div>
                          </Col>
                          <Col lg={9}>
                            <div className="reviewscontentmain">
                              <h2 className="reviewusername">
                                Vaishali and Dharmesh
                              </h2>
                              <p className="reviewtime">About 15 hours</p>
                              <div className="reviewstarrating">
                                <InlineSVG src={RatingIcon} />
                                <InlineSVG src={RatingIcon} />
                                <InlineSVG src={RatingIcon} />
                                <InlineSVG src={RatingIcon} />
                              </div>
                              <p className="reviewdescription">
                                Manoratha looked at our space and needs and
                                helped us reconnect with our area through
                                design. The choice of the floor color with
                                paints colors make our house look fresh. Our
                                living room accent wall is so unique that our
                                family and friends adore our living room because
                                of the accent wall. She designed beds with
                                storage in it that can fit about one bedroom
                                worth of stuff. She has an excellent knowledge
                                of lighting fixtures and can truly understand
                                your needs and concerns.
                              </p>
                            </div>
                          </Col>
                        </Row>
                      </div>
                    );
                  })}
                </Slider>
              </div>
            </Col>
          </Row>
        </Container>
      </section>
    )
  );
};

ReviewSliderComponent.propTypes = {
  imageArr: PropTypes.array.isRequired,
  isDotsAvail: PropTypes.bool.isRequired,
  isArrowShow: PropTypes.bool.isRequired,
};

export default ReviewSliderComponent;
