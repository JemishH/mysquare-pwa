import React, { useEffect, useState } from "react";
import LoaderComponent from "../LoaderComponent";

const Layout = ({ children }) => {
  // states
  const [isLoadiing, setIsLoading] = useState(true);

  // Life cycle hooks
  useEffect(() => {
    setTimeout(() => {
      setIsLoading(false);
    }, 4000);
  }, []);

  return (
    <div className="main">
      <LoaderComponent isLoading={isLoadiing} data-aos="fade-up" />
      <div>{children}</div>
    </div>
  );
};

export default Layout;
