import React, { useEffect, useRef } from "react";
import "styles/component/newsLetterComponent.module.scss";
import { Container, Row, Col } from "react-bootstrap";
import Aos from "aos";
import "aos/dist/aos.css";
import { apiService } from "../../services";
import { isValidEmail } from "../../utils/globalFunction";
import { errorAlert, successAlert } from "../../utils/alert";
const NewsLetterComponent = () => {
  // state hooks
  const inputEl = useRef(null);

  // Life cycle hooks
  useEffect(() => {
    Aos.init({
      offset: 200,
      duration: 1500,
      easing: "ease-in-sine",
      delay: 100,
    });
  }, []);

  // Api methods
  const handleSubmit = async (e) => {
    e.preventDefault();
    const trimEmail = inputEl.current.value.trim();
    if (checkValidation(trimEmail)) {
      const params = {
        email: trimEmail,
      };

      apiService.PostRequest("/subscribe", params, false).then(async (data) => {
        if (data) {
          successAlert("Success! 🎉 You are now subscribed to the newsletter.");
          inputEl.current.value = "";
        } else {
          errorAlert(data.message);
        }
      });
    }
  };

  const checkValidation = (trimEmail) => {
    if (trimEmail.length === 0) {
      errorAlert("Please enter a email address");
      return false;
    }
    if (!isValidEmail(trimEmail)) {
      errorAlert("Please enter a valid email address");
      return false;
    }
    return true;
  };

  return (
    <section
      className="ms-news-letter-main-section"
      data-aos="fade-left"
      data-aos-delay="100"
    >
      <Container>
        <Row>
          <Col md={12}>
            <div className="ms-news-letter-content-main">
              <div className="ms-news-letter-title">
                Interested in our Latest Projects and Updates
              </div>
              <div className="ms-news-letter-desc">
                Get the latest updates about our accomplishments and reviews.
                Subscribe to our news letter now!
              </div>
            </div>
            <div className="ms-news-letter-input-group-wrapper">
              <div className="ms-news-letter-input">
                <input
                  type="text"
                  name="email"
                  placeholder="Your Email"
                  required
                  ref={inputEl}
                />
              </div>
              <div className="ms-news-letter-button-wrapper">
                <button className="ms-button" onClick={handleSubmit}>
                  Submit
                </button>
              </div>
            </div>
          </Col>
        </Row>
      </Container>
    </section>
  );
};

export default NewsLetterComponent;
