import React, { useEffect, useState } from "react";
import "styles/component/contactcomponent.module.scss";
import { Container, Row, Col } from "react-bootstrap";
import Aos from "aos";
import "aos/dist/aos.css";
import { errorMessages, successMessages } from "../../utils/constant";
import { isValidEmail } from "../../utils/globalFunction";
import { apiService } from "../../services";
import { errorAlert, successAlert } from "../../utils/alert";

const ContactComponent = () => {
  // States
  // const [isLoading, setIsLoading] = useState(false)
  const [email, setEmail] = useState("");
  const [name, setName] = useState("");
  const [message, setMessage] = useState("");

  // Life Cycle Methods
  useEffect(() => {
    Aos.init({
      offset: 200,
      duration: 1500,
      easing: "ease-in-sine",
      delay: 100,
    });
  }, []);

  // api methods
  const handleSubmit = async () => {
    const trimName = name.trim();
    const trimEmail = email.trim();
    const trimMessage = message.trim();
    if (checkValidation(trimName, trimEmail, trimMessage)) {
      const params = {
        name: trimName,
        email: trimEmail,
        message: trimMessage,
      };
      apiService
        .PostRequest("/contactUs", params, true)
        .then(async (response) => {
          if (response && response.isSuccess) {
            setName("");
            setEmail("");
            setMessage("");
            successAlert(successMessages.msgSentSuccess);
          } else {
            errorAlert(response.message);
          }
        });
    }
  };

  const checkValidation = (name, email, message) => {
    if (name.length === 0) {
      errorAlert(errorMessages.nameRequired);
      return false;
    }
    if (email.length === 0) {
      errorAlert(errorMessages.emailRequired);
      return false;
    }
    if (!isValidEmail(email)) {
      errorAlert(errorMessages.validEmail);
      return false;
    }
    if (message.length === 0) {
      errorAlert(errorMessages.messageRequired);
      return false;
    }
    return true;
  };

  const onChangeField = (e) => {
    const { name, value } = e.target;
    if (name === "name") {
      setName(value);
    } else if (name === "email") {
      setEmail(value);
    } else if (name === "message") {
      setMessage(value);
    }
  };

  return (
    <section className="contactmain" data-aos="fade-right" data-aos-delay="100">
      <Container>
        <Row>
          <Col md={12}>
            <div className="contactcontentmain">
              <div className="contacttitle">
                We’d love to discuss your requirements
              </div>
              <div className="contactdescr">
                Fill the form below and we will contact you to discuss creative
                and fulfilling solutions for your space.
              </div>
            </div>
            <div className="contactinputmain">
              <div className="contactinput">
                <input
                  type="text"
                  name="name"
                  value={name}
                  onChange={onChangeField}
                  placeholder="Your Name"
                />
              </div>
              <div className="contactinput">
                <input
                  type="text"
                  name="email"
                  value={email}
                  onChange={onChangeField}
                  placeholder="Your Email"
                />
              </div>
              <div className="contactinput">
                <textarea
                  name="message"
                  value={message}
                  onChange={onChangeField}
                  placeholder="Your Message"
                />
              </div>

              <div className="contactbuttonmain">
                <button className={"ms-button"} onClick={handleSubmit}>
                  <span>Submit</span>
                </button>
              </div>
            </div>
          </Col>
        </Row>
      </Container>
    </section>
  );
};

ContactComponent.propTypes = {};

export default ContactComponent;
