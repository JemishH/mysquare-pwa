import React, { useEffect } from "react";
import "styles/component/project_secret_component.module.scss";
import { Container, Row, Col } from "react-bootstrap";
import Aos from "aos";
import ProgressiveNextImage from "components/ProgressiveNextImageComponent";
import "aos/dist/aos.css";

const ProjectSecetsComponent = () => {
  useEffect(() => {
    Aos.init({
      offset: 200,
      duration: 1500,
      easing: "ease-in-sine",
      delay: 100,
    });
  }, []);
  return (
    <section
      className="secratecontentmainsection"
      data-aos="fade-up"
      data-aos-delay="100"
    >
      <Container>
        <Row>
          <Col md={7}>
            <div className="secratecontentmain">
              <h1>
                OUR <span>PROJECTS SECRET</span>
              </h1>
              <div className="subtitle">
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do.
              </div>
              <div className="project_description">
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                eiusmod tempor incididunt ut labore et dolore.Lorem ipsum dolor
                sit amet, consectetur adipiscing elit, sed do eiusmod tempor
                incididunt ut labore et dolore.Lorem ipsum dolor sit amet.
              </div>
            </div>
          </Col>
          <Col md={5}>
            <div className="secretimgmain">
              <ProgressiveNextImage
                imageSrc={"/assets/images/project_01.png"}
              />

              {/* <ImageComponent url="assets/images/project_01.png" /> */}
            </div>
          </Col>
        </Row>

        <Row>
          <Col md={4}>
            <div className="listmain">
              <div className="projecttitle">Lorem ipsum dolor</div>
              <div className="projectdetail">
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                eiusmod tempor incididunt ut lab et dolore.Lorem ipsum dolor sit
                amet, consectetur adipiscing elit.
              </div>
            </div>
          </Col>
          <Col md={4}>
            <div className="listmain">
              <div className="projecttitle">Lorem ipsum dolor</div>
              <div className="projectdetail">
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                eiusmod tempor incididunt ut lab et dolore.Lorem ipsum dolor sit
                amet, consectetur adipiscing elit.
              </div>
            </div>
          </Col>
          <Col md={4}>
            <div className="listmain">
              <div className="projecttitle">Lorem ipsum dolor</div>
              <div className="projectdetail">
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                eiusmod tempor incididunt ut lab et dolore.Lorem ipsum dolor sit
                amet, consectetur adipiscing elit.
              </div>
            </div>
          </Col>
        </Row>
      </Container>
    </section>
  );
};

ProjectSecetsComponent.propTypes = {};

export default ProjectSecetsComponent;
