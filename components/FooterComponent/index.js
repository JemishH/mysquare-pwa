import React from "react";
import "styles/component/footercomponent.module.scss";
import { Container, Row, Col } from "react-bootstrap";
import Link from "next/link";
import ProgressiveNextImage from "components/ProgressiveNextImageComponent";

const FooterComponent = () => {
  return (
    <section className="footermain">
      <Container>
        <Row>
          <Col md={3}>
            <div className="brandlogofooter">
              <Link href="/">
                <a rel="noopener">
                  <ProgressiveNextImage
                    imageSrc={"/assets/images/mySquare.png"}
                  />
                  {/* <ImageComponent url={mySqureLogo} /> */}
                </a>
              </Link>
            </div>
          </Col>
          <Col md={3}>
            <div className="footercontentmain">
              <div className="footerlable">
                <span />
                Name
              </div>
              <div className="footercontent">Manoratha Patel</div>
            </div>
          </Col>
          <Col md={3}>
            <div className="footercontentmain">
              <div className="footerlable">
                <span />
                E-mail
              </div>

              <div className="footercontent">
                <a
                  href="mailto:manoratha@mysquare.us?Subject=Hello"
                  target="_blank"
                  rel="noreferrer"
                >
                  manoratha@mysquare.us
                </a>
              </div>
            </div>
          </Col>
          <Col md={3}>
            <div className="footercontentmain">
              <div className="footerlable">
                <span />
                Call
              </div>
              <div className="footercontent">+(408) 807-3120</div>
            </div>
          </Col>
        </Row>
        <Row>
          <Col md={12}>
            <div className="bottomfooter">© 2020 My Square LLC.</div>
          </Col>
        </Row>
      </Container>
    </section>
  );
};

export default FooterComponent;
