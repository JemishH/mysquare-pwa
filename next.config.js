const path = require("path");
const withCSS = require("@zeit/next-css");
const withSass = require("@zeit/next-sass");
const withImages = require("next-images");
const withPWA = require("next-pwa");
const runtimeCaching = require("next-pwa/cache");
const isDisablePWA = process.env.PROJECT_ENV !== "production";

module.exports = withPWA(
  withCSS(
    withSass(
      withImages({
        images: {
          domains: ["localhost", "dev.mysquare.us", "mysquare.us"],
        },
        webpack: (config) => {
          config.resolve.alias.components = path.join(__dirname, "components");
          config.resolve.alias.public = path.join(__dirname, "public");
          config.resolve.alias.utils = path.join(__dirname, "utils");
          config.resolve.alias.styles = path.join(__dirname, "styles");
          config.resolve.alias.assets = path.join(__dirname, "assets");
          config.resolve.alias.services = path.join(__dirname, "services");
          return config;
        },
        pwa: {
          dest: "public",
          disable: isDisablePWA,
          register: true,
          skipWaiting: true,
          runtimeCaching,
        },
      })
    )
  )
);
