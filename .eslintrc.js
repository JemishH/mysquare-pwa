module.exports = {
  env: {
    browser: true,
    es2021: true,
  },
  extends: ["plugin:react/recommended", "standard", "prettier"],
  parserOptions: {
    ecmaFeatures: {
      jsx: true,
    },
    ecmaVersion: 12,
    sourceType: "module",
  },
  settings: {
    react: {
      version: "detect",
    },
  },
  globals: {
    _: true,
    CSSModule: true,
    Streamy: true,
    ReactClass: true,
    SyntheticKeyboardEvent: true,
  },
  plugins: ["react", "unused-imports"],
  rules: {
    "linebreak-style": ["error", "unix"],
    "react/prop-types": "off",
    "unused-imports/no-unused-imports": "error",
    "unused-imports/no-unused-vars": [
      "error",
      {
        vars: "all",
        varsIgnorePattern: "^_",
        args: "after-used",
        argsIgnorePattern: "^_",
        caughtErrorsIgnorePattern: "^ignore",
      },
    ],
    // "no-unused-vars": [
    //   "error",
    //   {
    //     vars: "local",
    //     args: "after-used",
    //     argsIgnorePattern: "^_",
    //     caughtErrorsIgnorePattern: "^ignore",
    //   },
    // ],
    semi: ["error", "always"],
    // allow console and debugger in development
    "no-console": process.env.NODE_ENV === "production" ? 2 : 0,
    "no-debugger": process.env.NODE_ENV === "production" ? 2 : 0,
  },
};
