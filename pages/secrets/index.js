import React, { useEffect } from "react";
import MetaSEO from "components/SEOComponent";
import PropTypes from "prop-types";

import HeaderComponent from "components/HeaderComponent";
import FooterComponent from "components/FooterComponent";
import ContactComponent from "components/ContactComponent";
import NewsLetterComponent from "components/NewsletterComponent";
import ProjectSecetsComponent from "components/ProjectSecetsComponent";
const OurProjectSecret = (props) => {
  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);
  const renderSeo = () => <MetaSEO metaDetail={"sdsdsds"} />;
  return (
    <div key={props}>
      {renderSeo()}
      <HeaderComponent pathName={props.pathname} />
      <ProjectSecetsComponent />
      <ContactComponent />
      <NewsLetterComponent />
      <FooterComponent pathName={props.pathname} />
    </div>
  );
};

// Specifies the default values for props:
OurProjectSecret.defaultProps = {
  pathname: "/secrets",
};

OurProjectSecret.propTypes = {
  pathname: PropTypes.string.isRequire,
};

export async function getServerSideProps(context) {
  const params = {
    pathname: context.resolvedUrl,
  };
  return {
    props: params,
  };
}

export default OurProjectSecret;
