import React from "react";
import MetaSEO from "components/SEOComponent";
import ProjectDetailComponent from "components/ProjectDetailComponent";
import HeaderComponent from "components/HeaderComponent";
import FooterComponent from "components/FooterComponent";
import ContactComponent from "components/ContactComponent";
import PropTypes from "prop-types";
import NewsLetterComponent from "components/NewsletterComponent";

const ProjectDetail = (props) => {
  const renderSeo = () => <MetaSEO metaDetail={"sdsdsds"} />;
  return (
    <div key={props}>
      {renderSeo()}
      <HeaderComponent pathName={props?.pathname} />
      <ProjectDetailComponent pathName={props?.pathname} query={props?.query} />
      <ContactComponent />
      <NewsLetterComponent />
      <FooterComponent pathName={props.pathname} />
    </div>
  );
};

// Specifies the default values for props:
ProjectDetail.propTypes = {
  pathname: PropTypes.string.isRequired,
  query: PropTypes.object.isRequired,
};

export async function getServerSideProps(context) {
  const params = {
    pathname: `project/${context?.query?.project}`,
    query: context?.query,
  };
  return {
    props: params,
  };
}

export default ProjectDetail;
