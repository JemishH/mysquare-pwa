import React, { useEffect } from "react";
import MetaSEO from "components/SEOComponent";
import ProjectListComponent from "components/ProjectListComponent";
import HeaderComponent from "components/HeaderComponent";
import FooterComponent from "components/FooterComponent";
import ContactComponent from "components/ContactComponent";
import NewsLetterComponent from "components/NewsletterComponent";
import PropTypes from "prop-types";
import { staticProjectListData } from "../../utils/constant";

const ProjectListPage = (props) => {
  // Life cycle hooks
  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);

  // Render methods
  const renderSeo = () => <MetaSEO metaDetail={"sdsdsds"} />;

  return (
    <div key={props}>
      {renderSeo()}
      <HeaderComponent pathName={props?.pathname} />
      <ProjectListComponent
        pathName={props.pathname}
        projects={staticProjectListData}
      />
      <ContactComponent />
      <NewsLetterComponent />
      <FooterComponent pathName={props.pathname} />
    </div>
  );
};
// Specifies the default values for props:
ProjectListPage.defaultProps = {
  pathname: "/projects",
};

ProjectListPage.propTypes = {
  pathname: PropTypes.string,
};

export async function getServerSideProps(context) {
  const params = {
    pathname: context.resolvedUrl,
  };
  return {
    props: params,
  };
}
export default ProjectListPage;
