import React from "react";
import MetaSEO from "components/SEOComponent";
import dynamic from "next/dynamic";
import { sliderTestImages } from "utils/constant";
import PropTypes from "prop-types";
import { metaDetail, staticProjectListData1 } from "../utils/constant";
// import AboutServicesComponent from "components/AboutServicesComponent";
const AboutServicesComponent = dynamic(() =>
  import("components/AboutServicesComponent")
);

// import ProjectListComponent from "components/ProjectListComponent";
const ProjectListComponent = dynamic(() =>
  import("components/ProjectListComponent")
);

// import BannerComponent from "components/BannerComponent";
const BannerComponent = dynamic(() => import("components/BannerComponent"));

// import FooterComponent from "components/FooterComponent";
const FooterComponent = dynamic(() => import("components/FooterComponent"));

// import ContactComponent from "components/ContactComponent";
const ContactComponent = dynamic(() => import("components/ContactComponent"));

// import NewsLetterComponent from "components/NewsletterComponent";
const NewsLetterComponent = dynamic(() =>
  import("components/NewsletterComponent")
);

// import HowitWorkComponent from "components/HowitWorkComponent";
const HowitWorkComponent = dynamic(() =>
  import("components/HowitWorkComponent")
);

// import ReviewSliderComponent from "components/ReviewSliderComponent";
const ReviewSliderComponent = dynamic(() =>
  import("components/ReviewSliderComponent")
);
const SliderComponent = dynamic(() => import("components/SliderComponent"));

// import SliderComponent from "components/SliderComponent";

const LandingPage = (props) => {
  // Render methods
  const renderSeo = () => <MetaSEO metaDetail={metaDetail.landing} />;

  return (
    <div key={props}>
      {renderSeo()}
      {/* First banner section */}
      <BannerComponent pathName={props.pathname} />
      {/* Project listing section */}
      <ProjectListComponent
        pathName={props.pathname}
        projects={staticProjectListData1}
      />

      <HowitWorkComponent />
      <AboutServicesComponent />
      <SliderComponent
        imageArr={sliderTestImages}
        isDotsAvail={false}
        isArrowShow={true}
      />
      <ReviewSliderComponent
        imageArr={sliderTestImages}
        isDotsAvail={false}
        isArrowShow={true}
      />
      <ContactComponent />
      <NewsLetterComponent />
      <FooterComponent pathName={props.pathname} />
    </div>
  );
};
// Specifies the default values for props:
LandingPage.defaultProps = {
  pathname: "/",
};

LandingPage.propTypes = {
  pathname: PropTypes.string,
};

export async function getServerSideProps(context) {
  const params = {
    pathname: context.resolvedUrl,
  };
  return {
    props: params,
  };
}

export default LandingPage;
