import mailchimp from "@mailchimp/mailchimp_marketing";

mailchimp.setConfig({
  apiKey: process.env.NEXT_PUBLIC_MAILCHIMP_API_KEY,
  server: process.env.NEXT_PUBLIC_MAILCHIMP_API_SERVER,
});

export default async (req, res) => {
  const { email } = req.body;
  if (!email) {
    return res
      .status(400)
      .json({ isSuccess: false, message: "Email is required" });
  }

  try {
    await mailchimp.lists.addListMember(
      process.env.NEXT_PUBLIC_MAILCHIMP_AUDIENCE_ID,
      {
        email_address: email,
        status: "subscribed",
      }
    );
    return res.status(200).json({ data: { email: email }, status: 200 });
  } catch (error) {
    return res.status(500).json({ message: error.message || error.toString() });
  }
};
