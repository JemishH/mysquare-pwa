import { errorMessages } from "../../utils/constant";
import { isValidEmail } from "../../utils/globalFunction";
const nodemailer = require("nodemailer");

export default function (req, res) {
  // require("dotenv").config();
  const { email, message, name } = req.body;
  if (name && name.length === 0) {
    return res
      .status(404)
      .json({ isSuccess: false, message: errorMessages.nameRequired });
    // .end();
  }
  if (email && email.length === 0) {
    return res
      .status(404)
      .json({ isSuccess: false, message: errorMessages.emailRequired });
  }
  if (!isValidEmail(email)) {
    return res
      .status(404)
      .json({ isSuccess: false, message: errorMessages.validEmail });
  }
  if (message && message.length === 0) {
    return res
      .status(404)
      .json({ isSuccess: false, message: errorMessages.messageRequired });
  }

  try {
    const transporter = nodemailer.createTransport({
      port: 465,
      // host: "smtp.gmail.com",
      host: "smtp.sendgrid.net",
      secure: true, // use TLS
      auth: {
        user: "apikey",
        pass: process.env.NEXT_PUBLIC_SENDGRID_PASS,
      },
    });

    const mailData = {
      from: process.env.NEXT_PUBLIC_SENDGRID_FROM_ADDRESS,
      to: email,
      subject: `Message From ${req.body.name}`,
      text: req.body.message + " | Sent from: " + req.body.email,
      html: `<div>${req.body.message}</div><p>Sent from: ${req.body.email}</p>`,
    };

    transporter.sendMail(mailData, function (err, info) {
      if (err) {
        // console.log(err);
        res.send({
          status: 403,
          message: err,
        });
      } else {
        res.send({
          ...info,
          status: 200,
          mail: nodemailer.getTestMessageUrl(info),
        });
      }
    });
  } catch (error) {
    res.send({
      status: 403,
      message: "Something went wrong when sending mail.",
    });
  }
}
