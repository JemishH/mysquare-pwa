import React from "react";
import "../styles/combine.scss";

import Head from "next/head";
import PropTypes from "prop-types";
import Layout from "../components/Layout";

function MyApp({ Component, pageProps }) {
  console.log("process.", process.env.NEXT_PUBLIC_PROJECT_ENV);
  return (
    <>
      <Head>
        <meta
          name="viewport"
          content="width=device-width, initial-scale=1.0, maximum-scale=1.0,user-scalable=0"
        />
        {/* Theme color */}
        <meta name="theme-color" content="#41199b" />

        <link
          href="https://fonts.googleapis.com/css2?family=Josefin+Sans:wght@300;400;500;600;700&display=swap"
          rel="stylesheet"
        ></link>
        <link
          rel="stylesheet"
          href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css"
          crossOrigin="anonymous"
        />

        {/* link manifest.json */}
        <link rel="manifest" href="manifest.json" />

        {/* Fancy Box  */}
        <link
          rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.css"
          integrity="sha512-H9jrZiiopUdsLpg94A333EfumgUBpO9MdbxStdeITo+KEIMaNfHNvwyjjDJb+ERPaRS6DpyRlKbvPUasNItRyw=="
          crossOrigin="anonymous"
        />

        {/* Bootsrap-script and Jquery */}
        <script
          src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"
          crossOrigin="anonymous"
        />

        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" />
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.min.js" />
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js" />
        <script
          src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.js"
          crossOrigin="anonymous"
        />
        {/* Fancy Box  */}
        <script
          src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.js"
          crossOrigin="anonymous"
        />
        {/* react slick slider css */}
        <link
          rel="stylesheet"
          type="text/css"
          charSet="UTF-8"
          href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.min.css"
        />
        <link
          rel="stylesheet"
          type="text/css"
          href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick-theme.min.css"
        />

        {/* lazy load */}
        <script
          src="/vendor/lazysizes.min.js"
          strategy="lazyOnload"
          type="text/javascript"
          defer
        />
        <script
          src="/vendor/ls.unveilhooks.min.js"
          strategy="lazyOnload"
          type="text/javascript"
          defer
        />
        <script
          dangerouslySetInnerHTML={{
            __html: `
              window.lazySizesConfig = window.lazySizesConfig || {};
              window.lazySizesConfig.loadMode = 0
              window.lazySizesConfig.expand = 0
              window.lazySizesConfig.expFactor = 0
              window.lazySizesConfig.hFac = 0

              document.addEventListener("DOMContentLoaded", function () {
                console.warn("Dom Content Loaded");
              });
            `,
          }}
        />
      </Head>
      <Layout>
        <Component {...pageProps} />
      </Layout>
    </>
  );
}

MyApp.propTypes = {
  Component: PropTypes.any,
  pageProps: PropTypes.any,
};
export default MyApp;
